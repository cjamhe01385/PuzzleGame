//
//  LotteryUpgradeItem.cpp
//  RpgGame
//
//  Created by Yu-Shuan on 12/10/2017.
//
//

#include "LotteryUpgradeItem.hpp"
#include "ImageFileData.hpp"


bool LotteryUpgradeItem::init() {
  
  if(!Sprite::init()) {
    return false;
  }
  
  this->setAnchorPoint(Vec2(0, 0.5));
  this->setTexture(ImageFileData::LOTTERY_ITEM_UPGRADE_BK);
  
  _text = ui::Text::create();
  _text->setFontSize(30);
  
  _text->setPosition(Vec2(this->getContentSize().width/2, this->getContentSize().height * 0.35));
  _text->setString("test");
  
  this->addChild(_text);
  
  return true;
}

void LotteryUpgradeItem::update(std::string school) {
  _text->setString(school);
}
