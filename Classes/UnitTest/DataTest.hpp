//
//  DataTest.hpp
//  RpgGame
//
//  Created by Yu-Shuan on 30/09/2017.
//
//

#ifndef DataTest_hpp
#define DataTest_hpp

#include <stdio.h>
#include "gtest/gtest.h"
#include "SchoolData.hpp"
#include "SchoolTable.hpp"
#include "LotteryValidModel.hpp"

class ScoolDataEnv : public ::testing::Test {
  
  protected :
  void SetUp();
  SchoolData _testData;

};

class IntegrationScoolEnv : public ::testing::Test {
  
  protected :
  void SetUp();
  SchoolTable _testTable;
  PropertyTable _table;
  
};

class ModelEnv : public ::testing::Test {
  
  protected :
  void SetUp();
  LotteryValidModel _model;
  std::array<PropertyType, Config::MAX_CUBS_NUMBER> _test;

};


#endif /* DataTest_hpp */
