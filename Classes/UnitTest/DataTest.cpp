//  Created by Yu-Shuan on 30/09/2017.
//
#include "DataTest.hpp"
#include "Config.hpp"
#include "TimeTick.hpp"
#include <memory>

void ScoolDataEnv::SetUp() {
  _testData = SchoolData("School1", 10, 20, 30, 40);
}

void IntegrationScoolEnv::SetUp() {
  _testTable = SchoolTable();
  _table = PropertyTable();
  _testTable.init();
  _table.init(40, 40, 40, 40);
  
}

void ModelEnv::SetUp() {
  _model = LotteryValidModel();
  
  // Test data
  _test = {PropertyType::VITALITY, PropertyType::WISDOM, PropertyType::REACTION, PropertyType::WISDOM, PropertyType::ATTENTION,
           PropertyType::VITALITY,PropertyType::WISDOM, PropertyType::WISDOM, PropertyType::REACTION, PropertyType::ATTENTION,
           PropertyType::WISDOM, PropertyType::WISDOM, PropertyType::REACTION, PropertyType::ATTENTION, PropertyType::ATTENTION,
           PropertyType::VITALITY, PropertyType::REACTION, PropertyType::REACTION, PropertyType::VITALITY, PropertyType::ATTENTION,
           PropertyType::VITALITY, PropertyType::REACTION, PropertyType::REACTION, PropertyType::VITALITY, PropertyType::ATTENTION};
  _model.setupPropertyWithManual(_test);
}

TEST_F(ScoolDataEnv, ScoolData) {
  
  // Test single property
  EXPECT_TRUE(_testData.match(PropertyType::WISDOM, 20));
  EXPECT_FALSE(_testData.match(PropertyType::ATTENTION, 10));
  EXPECT_TRUE(_testData.match(PropertyType::VITALITY, 50));
  
  // Test total
  EXPECT_FALSE(_testData.match(10, 10, 30, 40));
  EXPECT_TRUE(_testData.match(10, 20, 30, 40));
  EXPECT_FALSE(_testData.match(10, 10, 10, 0));
  EXPECT_TRUE(_testData.match(50, 50, 30, 40));
  
}

TEST_F(IntegrationScoolEnv, ScoolTable) {
  EXPECT_EQ("中華大學", _testTable.match(_table)->getName());
}

TEST_F(ModelEnv, Model_Valid_UnitTest) {
  _model._currentProperty = PropertyType::REACTION;
  EXPECT_FALSE(_model.validProperty(PropertyType::WISDOM));
  EXPECT_FALSE(_model.isValid(Config::MAX_CUBS_NUMBER + 1));
}

TEST_F(ModelEnv, Model_Link_UnitTest) {
  
  _model.linkToArray(2);
  EXPECT_EQ(1, static_cast<int>(_model._linkedIndex.size()));
  EXPECT_EQ(PropertyType::NONE , _model._board[2]);
}

TEST_F(ModelEnv, Model_Link_Test) {
  
  _model.linkSameProperty(1);
  EXPECT_EQ(5, static_cast<int>(_model._linkedIndex.size()));
  EXPECT_EQ(PropertyType::NONE , _model._board[6]);
}

TEST_F(ModelEnv, Model_Calculate_UnitTest) {
  
  EXPECT_EQ(0.5, _model.internalCalculated(5.0));
  
  EXPECT_EQ(1, _model.calculate(1));
  EXPECT_EQ(8, _model.calculate(5));
}

TEST_F(ModelEnv, Model_Calculate_Test) {
  _model.linkSameProperty(1);
  EXPECT_EQ(8, _model.calculate());
}


TEST(TimeTack, TimeTest) {

  auto time = TimeTick();
  
  time.tick();
  EXPECT_EQ(0, time.getTick());
  
  time.start();
  time.tick();
  EXPECT_EQ(1, time.getTick());
  
  time.pause();
  time.tick();
  EXPECT_EQ(1, time.getTick());
  
  time.continu();
  time.tick();
  EXPECT_EQ(2, time.getTick());
  
}



