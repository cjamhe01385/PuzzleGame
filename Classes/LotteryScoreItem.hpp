//
//  LotteryScoreItem.hpp
//  RpgGame
//
//  Created by Yu-Shuan on 24/09/2017.
//
//

#ifndef LotteryScoreItem_hpp
#define LotteryScoreItem_hpp

#include <stdio.h>
#include <iostream>
#include "ui/CocosGUI.h"


USING_NS_CC;


class LotteryScoreItem : public Layer {

  public :
  static LotteryScoreItem* create(std::string labelName);
  bool init(std::string labelName);
  std::string getTexturePath(std::string propertyName);
  void update(int number);
  std::string getLabel();
  ui::Text* getTextLabel(){  return _scoreLabel; }

  private:
  ui::Text* _scoreLabel;
};

#endif /* LotteryScoreItem_hpp */
