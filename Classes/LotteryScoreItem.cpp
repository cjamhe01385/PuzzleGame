//
//  LotteryScoreItem.cpp
//  RpgGame
//
//  Created by Yu-Shuan on 24/09/2017.
//
//

#include "LotteryScoreItem.hpp"
#include "ImageFileData.hpp"

LotteryScoreItem* LotteryScoreItem::create(std::string labelName) {
  auto item = new LotteryScoreItem();
  if(item->init(labelName)) {
    item->autorelease();
    return item;
  }
  return nullptr;
}

bool LotteryScoreItem::init(std::string labelName) {

  if(!Layer::init()){
    return false;
  }
  
  this->setAnchorPoint(Vec2::ZERO);
  
  auto size = Director::getInstance()->getVisibleSize();
  auto background = Sprite::create(getTexturePath(labelName));
  background->setScale(size.width * 0.245 / background->getContentSize().width);
  
  background->setAnchorPoint(Vec2::ZERO);
  background->setPosition(Vec2(0, 0));
  this->addChild(background);
  
  _scoreLabel = ui::Text::create("0", "arial.tff", 50);
  _scoreLabel->setTextColor(Color4B(122, 122, 122, 255));
  _scoreLabel->setPosition(Vec2(background->getBoundingBox().getMidX() , background->getBoundingBox().getMidY()));
  _scoreLabel->setCascadeColorEnabled(true);
  this->addChild(_scoreLabel);
  
  return true;
}

std::string LotteryScoreItem::getTexturePath(std::string propertyName) {
  if(propertyName.compare(PropertyName::PROPERTY_NAME_WISDOM) == 0) {
    return ImageFileData::LOTTERY_ITEM_BOARD_RED;
  }
  else if(propertyName.compare(PropertyName::PROPERTY_NAME_REACTION) == 0) {
    return ImageFileData::LOTTERY_ITEM_BOARD_GREEN;
  }
  else if(propertyName.compare(PropertyName::PROPERTY_NAME_ATTENTION) == 0) {
    return ImageFileData::LOTTERY_ITEM_BOARD_BLUE;
  }
  else if(propertyName.compare(PropertyName::PROPERTY_NAME_VITALITY) == 0) {
    return ImageFileData::LOTTERY_ITEM_BOARD_YELLOW;
  }
  else {
    return ImageFileData::LOTTERY_ITEM_BOARD_RED;
  }
}

void LotteryScoreItem::update(int number) {
  auto originScale = this->getScale();
  auto largeScale = ScaleTo::create(0.3f, originScale * 2);
  auto smallScale = ScaleTo::create(0.2f, originScale);
  auto action = Sequence::create(TintTo::create(0.05f, Color3B(255, 0, 0)), largeScale, smallScale, TintTo::create(0.05f, Color3B(87, 87, 87)), NULL);
  _scoreLabel->setString(std::to_string(number));
  _scoreLabel->runAction(action);
}


