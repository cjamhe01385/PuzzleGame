//  Created by Yu-Shuan on 04/07/2017.
//
//

#ifndef global_manager_hpp
#define global_manager_hpp

#include <stdio.h>

using namespace cocos2d;

namespace Controller{
  class Controller;
}
class ControllerFactory;
class GameData;
class GameManager : public Ref {

  public:
  GameManager(RefPtr<GameData> game_data, RefPtr<Director> director) : game_data_(game_data),director_(director){};
  void setDirector(RefPtr<Director>& director);
  void setGameData(RefPtr<GameData>& gameData);
  RefPtr<GameData> getGameData();
  RefPtr<Director> getDirector();
  void pushScene(RefPtr<Controller::Controller> controller);
  void replaceScene(RefPtr<Controller::Controller> controller);
  Scene* getScene();
  
  private:
  RefPtr<GameData> game_data_;
  RefPtr<Director> director_;
  RefPtr<Controller::Controller> current_controller_;
  RefPtr<ControllerFactory> controller_factory_;
};


#endif /* global_manager_hpp */
