
/**
 * @Author: Yu-Shuan
 */

#include "LotteryCubeLayer.hpp"
#include "ImageFileData.hpp"
#include "LotteryScoreItem.hpp"
#include "LotteryTitleItem.hpp"
#include "LotteryValidModel.hpp"

USING_NS_CC;

bool LotteryCubeLayer::init(){
  
  if(!Layer::init()){
    return false;
  }

  _cubeSprite = Vector<Sprite*>();
  
  return true;
}

void LotteryCubeLayer::setup(const std::array<PropertyType, Config::MAX_CUBS_NUMBER>& board){
  auto size = cocos2d::Director::getInstance()->getVisibleSize();
  auto origin = Director::getInstance()->getVisibleOrigin();
  float width = (size/ Config::MAX_CUBS_LINE).width * 0.9;
  
  int startX = size.width * 0.05;
  float scale = 0.0;
  
  for(int i = 0; i < Config::MAX_CUBS_NUMBER; i++){
    
    Sprite* cube =  setupCube(board.at(i));
    
    if(scale == 0.0){
      scale = width / cube->getContentSize().width;
    }
    
    if(i < Config::MAX_CUBS_LINE) {
      auto cubeFrame = Sprite::create(ImageFileData::LOTTERY_ITEM_CUBE_FRAME);
      cubeFrame->setScale(scale);
      cubeFrame->setAnchorPoint(Vec2::ZERO);
      cubeFrame->setPosition(Vec2(startX + (i % Config::MAX_CUBS_LINE * width),(i / Config::MAX_CUBS_LINE * width)));
      cubeFrame->setTag(i);
      this->addChild(cubeFrame);
    }
    
    cube->setScale(scale);
    cube->setCascadeOpacityEnabled(true);
    cube->setPosition(Vec2(startX + (i % Config::MAX_CUBS_LINE * width),(i / Config::MAX_CUBS_LINE * width)));
    //cube->setTag(i);
    
    _cubeSprite.insert(i, cube);
    this->addChild(cube);
  }
}

Sprite* LotteryCubeLayer::setupCube(PropertyType property) {
  Sprite* cube = Sprite::create();
  cube->setAnchorPoint(Vec2::ZERO);
  setupTexture(cube , property);
  return cube;
}

void LotteryCubeLayer::setupTexture(Sprite* cube, PropertyType property) {
  switch (property) {
  case PropertyType::WISDOM :
    cube->setTexture(ImageFileData::LOTTERY_ITEM_CUBE_RED);
    break;
  case PropertyType::ATTENTION :
    cube->setTexture(ImageFileData::LOTTERY_ITEM_CUBE_GREEN);
    break;
  case PropertyType::REACTION :
    cube->setTexture(ImageFileData::LOTTERY_ITEM_CUBE_BLUE);
    break;
  case PropertyType::VITALITY :
    cube->setTexture(ImageFileData::LOTTERY_ITEM_CUBE_YELLOW);
    break;
  default:
    break;
  }
}

const std::string& LotteryCubeLayer::getActivityTexture(PropertyType property) {
  switch (property) {
  case PropertyType::WISDOM :
    return ImageFileData::LOTTERY_ITEM_CUBE_RED_ACTIVITY;
    break;
  case PropertyType::ATTENTION :
    return ImageFileData::LOTTERY_ITEM_CUBE_GREEN_ACTIVITY;
    break;
  case PropertyType::REACTION :
    return ImageFileData::LOTTERY_ITEM_CUBE_BLUE_ACTIVITY;
    break;
  case PropertyType::VITALITY :
    return ImageFileData::LOTTERY_ITEM_CUBE_YELLOW_ACTIVITY;
    break;
  default:
    return ImageFileData::LOTTERY_ITEM_CUBE_RED_ACTIVITY;
    break;
  }
}

void LotteryCubeLayer::highlightAction(const std::vector<int>& linkedIndex, PropertyType property) {
  std::string activityImagePath = getActivityTexture(property);
  for(const auto& index : linkedIndex) {
    _cubeSprite.at(index)->setTexture(activityImagePath);
    //_cubeSprite.at(index)->setOpacity(100);
  }
}

void LotteryCubeLayer::cleanAction(const std::vector<int>& linkedIndex) {
   for(const auto& index : linkedIndex) {
     auto hideAction = FadeOut::create(0.2f);
     _cubeSprite.at(index)->runAction(hideAction);
     _cubeSprite.at(index)->setVisible(false);
   }
}

void LotteryCubeLayer::moveDownAction() {

  for(int i = 0; i < Config::MAX_CUBS_NUMBER ; i ++) {
     if(!_cubeSprite.at(i)->isVisible()) {
       swap(i, i + Config::MAX_CUBS_LINE);
     }
  }

}

void LotteryCubeLayer::swap(int index, int index2) {
  
  if(index2 >= Config::MAX_CUBS_NUMBER) {
    return;
  }
  
  auto cube1 = _cubeSprite.at(index);
  auto cube2 = _cubeSprite.at(index2);

  if(!cube2->isVisible()) {
    swap(index, index2 + Config::MAX_CUBS_LINE);
  }
  else {
    auto moveAction = MoveTo::create(0.2f, cube1->getPosition());
    auto vec2 = cube2->getPosition();
    cube2->runAction(moveAction);
    cube1->setPosition(vec2);
    
    cube2->setTag(index);
    cube1->setTag(index2);
    
    _cubeSprite.swap(cube1, cube2);
  }
}

void LotteryCubeLayer::resetAction(const std::array<PropertyType, 25>& board) {
  
  for(int i = 0; i < Config::MAX_CUBS_NUMBER ; i ++) {
    auto cube = _cubeSprite.at(i);
    if(!cube->isVisible()){
      auto delay = DelayTime::create(0.2f);
      auto fadin = FadeIn::create(0.2f);
      auto show = Show::create();
      setupTexture(cube, board.at(i));
      cube->runAction(Sequence::create(delay,show,fadin, NULL));
    }
  }
}

