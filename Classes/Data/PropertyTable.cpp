//  Created by Yu-Shuan on 30/09/2017.
//
//

#include "PropertyTable.hpp"
#include "SchoolData.hpp"

using namespace std;

void PropertyTable::init() {
  init(0,0,0,0);
}

void PropertyTable::init(int wistom, int reation, int attention, int vitality) {
  _properties.insert(make_pair(PropertyType::WISDOM, wistom));
  _properties.insert(make_pair(PropertyType::REACTION, reation));
  _properties.insert(make_pair(PropertyType::ATTENTION, attention));
  _properties.insert(make_pair(PropertyType::VITALITY, vitality));
  _properties.insert(make_pair(PropertyType::NONE, 0));
}

int PropertyTable::get(PropertyType type) {
  return _properties.find(type)->second;
}

int PropertyTable::set(PropertyType type, int score) {
  _properties.find(type)->second += score;
  return get(type);
}
