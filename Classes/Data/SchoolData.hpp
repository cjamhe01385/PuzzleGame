//  Created by Yu-Shuan on 25/09/2017.
//
//

#ifndef SchoolData_hpp
#define SchoolData_hpp

#include <stdio.h>
#include <iostream>
#include <string>
#include "EnumValue.hpp"
#include "PropertyTable.hpp"

class SchoolData {

  public :
  SchoolData(){};
  SchoolData(std::string name, int wistom, int reation, int attention, int vitality):
  _name(name),
  _table()
  {
    _table = PropertyTable();
    _table.init(wistom, reation, attention, vitality);
  };
  
  std::string getName(){ return _name; };
  bool match(PropertyTable& userProperty);
  bool match(int wistom, int reation, int attention, int vitality);
  bool match(PropertyType type, int number);
  
  private :
  std::string _name;
  PropertyTable _table;
};

#endif /* SchoolData_hpp */
