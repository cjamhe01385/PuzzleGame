//  Created by Yu-Shuan on 01/10/2017.
//
//
#include "SchoolTable.hpp"

void SchoolTable::init() {
  
  _schools.push_back(SchoolData("考生",0,0,0,0));
  _schools.push_back(SchoolData("南華大學",10,10,10,10));
  _schools.push_back(SchoolData("開南大學",20,20,20,20));
  _schools.push_back(SchoolData("真理大學",30,30,30,30));
  _schools.push_back(SchoolData("中華大學",40,40,40,40));
  _schools.push_back(SchoolData("大葉大學",50,50,50,50));

}

SchoolData* SchoolTable::match(PropertyTable& userProperty) {
  
  SchoolData* school = nullptr;
  int maxSize = static_cast<int>(_schools.size());
  
  for(int i = _currentIndex ; i < maxSize; i++ ){
    if(_schools.at(i).match(userProperty)) {
      school = &_schools.at(i);
      _currentIndex = i;
    }
    else {
      break;
    }
  }
  
  return school;
}
