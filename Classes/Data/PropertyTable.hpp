//  Created by Yu-Shuan on 30/09/2017.
//

#ifndef PropertyTable_hpp
#define PropertyTable_hpp

#include <stdio.h>
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include "EnumValue.hpp"

using namespace std;

class PropertyTable {

  public :
  void init();
  void init(int wistom, int reation, int attention, int vitality);
  int get(PropertyType type);
  int set(PropertyType type, int score);
  
  private :
  std::map<PropertyType, int> _properties;
  int _value;
};

#endif /* PropertyTable_hpp */
