//
//  SchoolData.cpp
//  RpgGame
//
//  Created by Yu-Shuan on 25/09/2017.
//
//

#include "SchoolData.hpp"

bool SchoolData::match(PropertyTable& userProperty) {
  return match(userProperty.get(PropertyType::WISDOM),
               userProperty.get(PropertyType::REACTION),
               userProperty.get(PropertyType::ATTENTION),
               userProperty.get(PropertyType::VITALITY));
}

bool SchoolData::match(int wistom, int reation, int attention, int vitality) {
  if(!match(PropertyType::WISDOM,wistom))
    return false;
  else if(!match(PropertyType::REACTION,reation))
    return false;
  else if(!match(PropertyType::ATTENTION,attention))
    return false;
  else if(!match(PropertyType::VITALITY,vitality))
    return false;
  else
    return true;
}

bool SchoolData::match(PropertyType type, int number) {

  return _table.get(type) <= number;
}
