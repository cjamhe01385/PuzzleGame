//
//  SchoolTable.hpp
//  RpgGame
//
//  Created by Yu-Shuan on 01/10/2017.
//
//

#ifndef SchoolTable_hpp
#define SchoolTable_hpp

#include <stdio.h>
#include <iostream>
#include <vector>
#include "SchoolData.hpp"

using namespace std;

class SchoolTable {

  public :
  void init();
  SchoolData* match(PropertyTable& userProperty);
  
  private :
  
  std::vector<SchoolData> _schools;
  int _currentIndex;

};

#endif /* SchoolTable_hpp */
