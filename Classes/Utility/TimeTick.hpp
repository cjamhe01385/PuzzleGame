//  Created by Yu-Shuan on 14/10/2017.
//
//

#ifndef TimeTick_hpp
#define TimeTick_hpp

#include <stdio.h>
#include <iostream>

class TimeTick {
  
  public :
  TimeTick() :
  _tick(0),
  _isStop(true),
  _isPause(false) {
  
  };
  
  bool tick();
  void reset();
  void pause();
  void stop();
  void start();
  void continu() { _isPause = false; };
  int getTick(){ return _tick; };
  
  private :
  
  int _tick;
  bool _isStop;
  bool _isPause;
};

#endif /* TimeTick_hpp */
