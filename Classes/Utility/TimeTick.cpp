//  Created by Yu-Shuan on 14/10/2017.
//
//

#include "TimeTick.hpp"
#include <thread>


bool TimeTick::tick() {
  if(!_isStop && !_isPause) {
    _tick += 1;
    
    return true;
  }
  return false;
}

void TimeTick::start() {
  _isStop = false;
}

void TimeTick::stop() {
  _isStop = true;
  reset();
}

void TimeTick::pause() {
  _isPause = true;
}

void TimeTick::reset() {
  _tick = 0;
}
