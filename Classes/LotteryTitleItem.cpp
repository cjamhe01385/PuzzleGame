//
//  LotteryTitleItem.cpp
//  RpgGame
//
//  Created by Yu-Shuan on 24/09/2017.
//
//

#include "LotteryTitleItem.hpp"
#include "ImageFileData.hpp"


USING_NS_CC;

bool LotteryTitleItem::init() {
  
  if(!Layer::init()) {
    return false;
  }
  
  auto size = Director::getInstance()->getVisibleSize();
  _background = Sprite::create(ImageFileData::LOTTERY_ITEM_TITLE_BK);
  _background->setScale(size.width / _background->getContentSize().width / 8);
  _background->setAnchorPoint(Vec2(0,0));
  _background->setPosition(Vec2(0,0));
  this->addChild(_background);
  
  _titleLabel = ui::Text::create("高中生", "arial.tff", 40);
  _titleLabel->setTextColor(Color4B(105, 105, 105, 255));
  _titleLabel->setAnchorPoint(Vec2(0, 0.5));
  _titleLabel->setPosition(Vec2(_background->getBoundingBox().getMaxX() + 10, _background->getBoundingBox().getMidY()));
  this->addChild(_titleLabel);
  
  return true;
}

void LotteryTitleItem::update(std::string& text) {
  _titleLabel->setString(text);
}
