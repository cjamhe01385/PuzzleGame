/**
 * The main scene of puzzle game.
 * A scene must contains at least a layer and the model.
 * @author Yu-Shuan
 */

#ifndef Lottery_hpp
#define Lottery_hpp

#include <stdio.h>
#include <iostream>
#include "ui/CocosGUI.h"
#include "EnumValue.hpp"
#include "LotteryValidModel.hpp"
#include "LotteryCubeLayer.hpp"
#include "LotteryTitleItem.hpp"
#include "LotteryUpgradeItem.hpp"
#include "TimeTick.hpp"

USING_NS_CC;

class LotteryScene : public LayerColor{

  public:
  CREATE_FUNC(LotteryScene);
  static Scene* createScene();
  bool init() override;

  private:
  void setupEvent();
  void setupBackground(Size& size);
  void setupUI(Size& size);
  void setupScoreItem(float x, float y, std::string text);
  
  void timeTick(float time);

  bool onTouchBegan(Touch *touch, Event *event) override;
  void onTouchEnded(Touch *touch, Event *event) override;
  bool isTouchOnNode(Touch *touch, Event *event);
  
  void showUpgrade();
  
  void updateScoreLabel(PropertyType type, int score);
  void dispatchEvent();
  
  LotteryValidModel _model;
  TimeTick _tick;
  
  LotteryCubeLayer* _layer;
  LotteryTitleItem* _title;
  LotteryUpgradeItem* _upgrade;
  
  EventListenerTouchOneByOne* _touchListener;
  
};
#endif /* Lottery_hpp */
