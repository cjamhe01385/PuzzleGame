//
//  LotteryUpgradeItem.hpp
//  RpgGame
//
//  Created by Yu-Shuan on 12/10/2017.
//
//

#ifndef LotteryUpgradeItem_hpp
#define LotteryUpgradeItem_hpp

#include <stdio.h>
#include "ui/CocosGUI.h"

USING_NS_CC;

class LotteryUpgradeItem : public Sprite {
  
  public :
  CREATE_FUNC(LotteryUpgradeItem);
  
  bool init() override;
  
  void update(std::string school);
  
  private :
  ui::Text* _text;
  
};


#endif /* LotteryUpgradeItem_hpp */
