//
//  EnumValue.hpp
//  RpgGame
//
//  Created by Yu-Shuan on 23/09/2017.
//
//

#ifndef EnumValue_hpp
#define EnumValue_hpp

#include <stdio.h>

enum class CubeColor {
  RED = 0,
  GREEN,
  BLUE,
  YELLOW
};

enum class PropertyType {
  WISDOM = 0,
  REACTION,
  ATTENTION,
  VITALITY,
  NONE
};

#endif /* EnumValue_hpp */
