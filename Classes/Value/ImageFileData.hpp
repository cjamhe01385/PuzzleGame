/**
 * The collection of required image paths.
 * @author Yu-Shuan
 */

#ifndef ImageFileData_hpp
#define ImageFileData_hpp

#include <stdio.h>
#include <string>

namespace ImageFileData {
  extern const std::string LOTTERY_SCENE_BACKGROUND;
  extern const std::string LOTTERY_ITEM_CUBE;
  extern const std::string LOTTERY_ITEM_CUBE_RED;
  extern const std::string LOTTERY_ITEM_CUBE_RED_ACTIVITY;
  extern const std::string LOTTERY_ITEM_CUBE_GREEN;
  extern const std::string LOTTERY_ITEM_CUBE_GREEN_ACTIVITY;
  extern const std::string LOTTERY_ITEM_CUBE_BLUE;
  extern const std::string LOTTERY_ITEM_CUBE_BLUE_ACTIVITY;
  extern const std::string LOTTERY_ITEM_CUBE_YELLOW;
  extern const std::string LOTTERY_ITEM_CUBE_YELLOW_ACTIVITY;
  extern const std::string LOTTERY_ITEM_BOARD_RED;
  extern const std::string LOTTERY_ITEM_BOARD_BLUE;
  extern const std::string LOTTERY_ITEM_BOARD_GREEN;
  extern const std::string LOTTERY_ITEM_BOARD_YELLOW;
  extern const std::string LOTTERY_ITEM_CUBE_FRAME;
  extern const std::string LOTTERY_ITEM_SCORE_BK;
  extern const std::string LOTTERY_ITEM_TITLE_BK;
  extern const std::string LOTTERY_ITEM_UPGRADE_BK;
}

namespace PropertyName {
  extern const std::string PROPERTY_NAME_WISDOM;
  extern const std::string PROPERTY_NAME_REACTION;
  extern const std::string PROPERTY_NAME_ATTENTION;
  extern const std::string PROPERTY_NAME_VITALITY;
}


#endif /* ImageFileData_hpp */
