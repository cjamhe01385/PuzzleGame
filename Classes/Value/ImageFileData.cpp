//
//  ImageFileData.cpp
//  RpgGame
//
//  Created by Yu-Shuan on 30/09/2017.
//
//

#include <stdio.h>
#include "ImageFileData.hpp"

const std::string ImageFileData::LOTTERY_SCENE_BACKGROUND = "bk.jpg";
const std::string ImageFileData::LOTTERY_ITEM_CUBE = "cube.png";
const std::string ImageFileData::LOTTERY_ITEM_CUBE_RED = "language-cube-normal.png";
const std::string ImageFileData::LOTTERY_ITEM_CUBE_RED_ACTIVITY = "language-cube-activity.png";
const std::string ImageFileData::LOTTERY_ITEM_CUBE_GREEN = "math-cube-normal.png";
const std::string ImageFileData::LOTTERY_ITEM_CUBE_GREEN_ACTIVITY = "math-cube-activity.png";
const std::string ImageFileData::LOTTERY_ITEM_CUBE_BLUE = "science-cube-normal.png";
const std::string ImageFileData::LOTTERY_ITEM_CUBE_BLUE_ACTIVITY = "science-cube-activity.png";
const std::string ImageFileData::LOTTERY_ITEM_CUBE_YELLOW = "society-cube-normal.png";
const std::string ImageFileData::LOTTERY_ITEM_CUBE_YELLOW_ACTIVITY = "society-cube-activity.png";
const std::string ImageFileData::LOTTERY_ITEM_BOARD_RED = "language-board.png";
const std::string ImageFileData::LOTTERY_ITEM_BOARD_BLUE = "science-board.png";
const std::string ImageFileData::LOTTERY_ITEM_BOARD_GREEN = "math-board.png";
const std::string ImageFileData::LOTTERY_ITEM_BOARD_YELLOW = "society-board.png";
const std::string ImageFileData::LOTTERY_ITEM_CUBE_FRAME = "cube_frame.png";
const std::string ImageFileData::LOTTERY_ITEM_SCORE_BK = "item_bk.png";
const std::string ImageFileData::LOTTERY_ITEM_TITLE_BK = "university-icon.png";
const std::string ImageFileData::LOTTERY_ITEM_UPGRADE_BK = "level-up.png";

const std::string PropertyName::PROPERTY_NAME_WISDOM = "Language";
const std::string PropertyName::PROPERTY_NAME_REACTION = "Mathematic";
const std::string PropertyName::PROPERTY_NAME_ATTENTION = "Science";
const std::string PropertyName::PROPERTY_NAME_VITALITY = "Society";
