//  Created by Yu-Shuan on 06/10/2017.
//
//

#ifndef Config_hpp
#define Config_hpp

#include <stdio.h>

class Config {

  public:
  static const int MAX_CUBS_LINE = 5;
  static const int MAX_CUBS_NUMBER = 25;

};

#endif /* Config_hpp */
