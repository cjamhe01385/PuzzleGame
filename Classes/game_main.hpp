//  Created by Yu-Shuan on 11/06/2017.

#ifndef game_main_hpp
#define game_main_hpp

#include <stdio.h>
#include <iostream>
#include "cocos2d.h"

using namespace cocos2d;

class GameMain {

  public:
    Scene* Run();
};

#endif /* game_main_hpp */
