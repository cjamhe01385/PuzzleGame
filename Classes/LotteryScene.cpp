#include "LotteryScene.hpp"
#include "ImageFileData.hpp"
#include "LotteryScoreItem.hpp"
#include "LotteryValidModel.hpp"
#include "TimeTick.hpp"
#include "Config.hpp"

USING_NS_CC;

Scene* LotteryScene::createScene(){
  auto scene = Scene::create();
  auto layer = LotteryScene::create();
  scene->addChild(layer);
  
  return scene;
}

bool LotteryScene::init(){
  
  if(!LayerColor::initWithColor(Color4B(242, 242, 242, 255))){
    return false;
  }
  
  _model = LotteryValidModel();
  _model.setup();
  _model.setupPropertyWithRandom();
  
  _tick = TimeTick();
  _tick.start();
  
  this->schedule(schedule_selector(LotteryScene::timeTick), 1.0f);
  
  auto size = cocos2d::Director::getInstance()->getVisibleSize();
  //setupBackground(size);
  setupUI(size);
  setupEvent();
  dispatchEvent();

  return true;
}

void LotteryScene::timeTick(float time) {
  _tick.tick();
  CCLOG("Time tick is %d", _tick.getTick());
}

void LotteryScene::setupBackground(Size& size) {
  auto origin = Director::getInstance()->getVisibleOrigin();
  auto background = cocos2d::Sprite::create(ImageFileData::LOTTERY_SCENE_BACKGROUND);
  background->setScale(size.width/background->getContentSize().width, size.height/background->getContentSize().height);
  background->setPosition(origin + size/2);
  this->addChild(background);
}

void LotteryScene::setupUI(cocos2d::Size &size){
  auto origin = Director::getInstance()->getVisibleOrigin();
  
  _title = LotteryTitleItem::create();
  _title->setName("title");
  _title->setPosition(origin.x + size.width * 0.05, origin.y + size.height - _title->getBackground()->getBoundingBox().getMaxY() - size.width * 0.05);
  this->addChild(_title);
  
  // initialize the layer with data retrieved from the model.
  _layer = LotteryCubeLayer::create();
  _layer->setup(_model.getBoard());
  _layer->setPosition(origin.x, size.height/3);
  this->addChild(_layer);
  
  _upgrade = LotteryUpgradeItem::create();
  _upgrade->setPosition(origin.x - size.width, origin.y + size.height * 0.22);
  _upgrade->setScale(size.width/ 2 / _upgrade->getContentSize().width, size.width/ 2/ _upgrade->getContentSize().width);
  this->addChild(_upgrade);
  
  this->setupScoreItem(origin.x, origin.y, PropertyName::PROPERTY_NAME_WISDOM);
  this->setupScoreItem(origin.x + size.width / 4 + size.width * 0.0025, origin.y, PropertyName::PROPERTY_NAME_REACTION);
  this->setupScoreItem(origin.x + size.width / 2 + size.width * 0.0025, origin.y , PropertyName::PROPERTY_NAME_ATTENTION);
  this->setupScoreItem(origin.x + size.width * 3 / 4 + size.width * 0.005, origin.y, PropertyName::PROPERTY_NAME_VITALITY);
}

void LotteryScene::setupScoreItem(float x, float y, std::string text) {
  auto item = LotteryScoreItem::create(text);
  item->setName(text);
  item->setPosition(x, y);
  this->addChild(item);
}

bool LotteryScene::onTouchBegan(Touch *touch, Event *event) {
  if(this->isTouchOnNode(touch, event)) {
    
    int touchedIndex = event->getCurrentTarget()->getTag();
    
    if(_model.isValid(touchedIndex)) {
      _model.linkSameProperty(touchedIndex);
      _layer->highlightAction(_model.getLinkedIndex(), _model.getCurrentProperty());
      return true;
    }
    else {
      return false;
    }
    
  }
  
  return false;
}

void LotteryScene::onTouchEnded(Touch *touch, Event *event) {
  if(this->isTouchOnNode(touch, event)) {
  
    int score = _model.calculate();
    updateScoreLabel(_model.getCurrentProperty(), score);
    // Hiden linked cubes.
    _layer->cleanAction(_model.getLinkedIndex());
    // Replace the position of linked cubes.
    _layer->moveDownAction();
    
    // Update the property that represents new cubes.
    _model.swap();
    _model.updateProperty();
    
    std::string schoolName = _model.getSchool()->getName();
    _title->update(schoolName);
    
    // If title upgraded.
    if(_model.isUpgrade()) {
      _upgrade->update(schoolName);
      showUpgrade();
    }
    
    // Update the new cubes to view.
    _layer->resetAction(_model.getBoard());
    _model.reset();
  }
}

bool LotteryScene::isTouchOnNode(Touch *touch, Event *event) {
  auto targetCube = event->getCurrentTarget();
  auto location = targetCube->convertToNodeSpace(touch->getLocation());
  Size s = targetCube->getContentSize();
  Rect rect = Rect(0, 0, s.width, s.height);
  if(rect.containsPoint(location)){
    return true;
  }
  else {
    return false;
  }
}

void LotteryScene::updateScoreLabel(PropertyType type, int score) {
  std::string childName = _model.getPropertyName(type);
  this->getChildByName<LotteryScoreItem*>(childName)->getTextLabel()->setString(std::to_string(score));
}

void LotteryScene::setupEvent(){
  auto cubes = _layer->getCubes();
  _touchListener = EventListenerTouchOneByOne::create();
  _touchListener->setSwallowTouches(true);
  _touchListener->onTouchBegan = CC_CALLBACK_2(LotteryScene::onTouchBegan, this);
  _touchListener->onTouchEnded = CC_CALLBACK_2(LotteryScene::onTouchEnded, this);
}

void LotteryScene::dispatchEvent() {
  _eventDispatcher->addEventListenerWithSceneGraphPriority(_touchListener, _layer->getChildByTag(0));
  _eventDispatcher->addEventListenerWithSceneGraphPriority(_touchListener->clone(), _layer->getChildByTag(1));
  _eventDispatcher->addEventListenerWithSceneGraphPriority(_touchListener->clone(), _layer->getChildByTag(2));
  _eventDispatcher->addEventListenerWithSceneGraphPriority(_touchListener->clone(), _layer->getChildByTag(3));
  _eventDispatcher->addEventListenerWithSceneGraphPriority(_touchListener->clone(), _layer->getChildByTag(4));
}

void LotteryScene::showUpgrade() {
  auto origin = Director::getInstance()->getVisibleOrigin();
  auto size = Director::getInstance()->getVisibleSize();

  auto moveInAction = MoveTo::create(0.2f, Vec2(origin.x, origin.y + size.height * 0.22));
  auto moveOutAction = MoveTo::create(0.2f, Vec2(origin.x - size.width, origin.y + size.height * 0.22));
  
  auto action = Sequence::create(moveInAction, DelayTime::create(3.0f), moveOutAction, NULL);
  _upgrade->runAction(action);
}

