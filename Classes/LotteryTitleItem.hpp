//
//  LotteryTitleItem.hpp
//  RpgGame
//
//  Created by Yu-Shuan on 24/09/2017.
//
//

#ifndef LotteryTitleItem_hpp
#define LotteryTitleItem_hpp

#include <stdio.h>
#include <iostream>
#include "ui/CocosGUI.h"

USING_NS_CC;

class LotteryTitleItem : public Layer{

  public :
  CREATE_FUNC(LotteryTitleItem);
  bool init() override;
  void update(std::string& text);
  Sprite* getBackground() {  return _background;};

  private :
  ui::Text* _titleLabel;
  Sprite* _background;
};

#endif /* LotteryTitleItem_hpp */
