//  Created by Yu-Shuan on 04/07/2017.
//
//

#include "global_manager.hpp"
#include "controller_delegate.hpp"

using namespace cocos2d;

void GameManager::setDirector(RefPtr<Director>& director) {
  this->director_ = director;
}

void GameManager::setGameData(RefPtr<GameData>& gameData) {
  this->game_data_ = gameData;
}

RefPtr<GameData> GameManager::getGameData() {
  return this->game_data_;
}

RefPtr<Director> GameManager::getDirector() {
  return this->director_;
}

void GameManager::pushScene(RefPtr<Controller::Controller> controller) {
  this->director_->pushScene(controller->getView());
}

void GameManager::replaceScene(RefPtr<Controller::Controller> controller) {
  this->director_->replaceScene(controller->getView());
}

Scene* GameManager::getScene(){
  return current_controller_->getView();
}


