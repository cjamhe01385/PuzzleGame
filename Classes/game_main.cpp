//  Created by Yu-Shuan on 11/06/2017.


#include "game_main.hpp"
#include "LotteryScene.hpp"

USING_NS_CC;

Scene* GameMain::Run() {
  auto scene = LotteryScene::createScene();
  return scene;
}
