//  Created by Yu-Shuan on 23/09/2017.
//

#include <ctime>
#include <math.h>
#include "LotteryValidModel.hpp"
#include "ImageFileData.hpp"

void LotteryValidModel::setup() {
  _schooTable = SchoolTable();
  _scoreTable = PropertyTable();
  
  _schooTable.init();
  _scoreTable.init();
  
  isUpgraded = false;
  _currentSchool = nullptr;
  
  srand((int)time(0));
  
}

PropertyType LotteryValidModel::intToProperty(int intNumber) {
  switch(intNumber) {
    case 0 :
      return PropertyType::WISDOM;
    case 1 :
      return PropertyType::ATTENTION;
    case 2 :
      return PropertyType::REACTION;
    case 3 :
      return PropertyType::VITALITY;
    default:
      return PropertyType::WISDOM;
  }
}

void LotteryValidModel::linkSameProperty(int boardIndex) {
  
  if(!isValid(boardIndex)) {
    boardIndex = 0;
  }
  _currentProperty = _board[boardIndex];
  
  link(boardIndex);
}

void LotteryValidModel::setupPropertyWithRandom() {

  for(int i = 0; i < Config::MAX_CUBS_NUMBER; i ++) {
    _board[i] = intToProperty(rand() % 4);
  }
}

void LotteryValidModel::setupPropertyWithManual(std::array<PropertyType, Config::MAX_CUBS_NUMBER>& board) {
  _board = board;
}

const std::vector<int>& LotteryValidModel::getLinkedIndex(){

  //CCASSERT(_linkedIndex.size() > 0, "Please use link() first.");
  return _linkedIndex;
}

void LotteryValidModel::link(int boardIndex) {
  
  if(validProperty(_board[boardIndex])) {
    
    // mark the visited index;
    linkToArray(boardIndex);
    
    // Check the boundary before do recursion.
    if (boardIndex % Config::MAX_CUBS_LINE != 0) link(boardIndex - 1);
    if (boardIndex % Config::MAX_CUBS_LINE != (Config::MAX_CUBS_LINE - 1)) link(boardIndex + 1);
    if (boardIndex + Config::MAX_CUBS_LINE < Config::MAX_CUBS_NUMBER) link(boardIndex + Config::MAX_CUBS_LINE);
    if (boardIndex - Config::MAX_CUBS_LINE >= 0) link(boardIndex - Config::MAX_CUBS_LINE);
  }
  
}

bool LotteryValidModel::validProperty(PropertyType comparedProperty) {
  return (comparedProperty == _currentProperty && comparedProperty != PropertyType::NONE) ? true : false;
}

void LotteryValidModel::linkToArray(int boardIndex) {
  _linkedIndex.push_back(boardIndex);
  _board[boardIndex] = PropertyType::NONE;
}

SchoolData* LotteryValidModel::getSchool() {

  SchoolData* school;
  school = _schooTable.match(_scoreTable);
  
  if(school != _currentSchool) {
    UpgradeTrue();
    _currentSchool = school;
  }
  
  #ifdef _D_COCOS2D_TEST
  
  CCLOG("The current school is : %s", _currentSchool->getName().c_str());
  
  #endif
  
  return _currentSchool;
}

void LotteryValidModel::reset() {

  if(_linkedIndex.size() == 0) {
    return;
  }
  
  UpgradeFalse();
  _linkedIndex.clear();
  _currentProperty = PropertyType::NONE;
}

void LotteryValidModel::updateProperty() {

  for(int i = 0; i < Config::MAX_CUBS_NUMBER ; i++ ) {
    if(_board[i] == PropertyType::NONE){
      _board[i] = intToProperty(rand() % 4);
    }
  }
}

bool LotteryValidModel::isValid(int index) {
  return (index >= 0 && index < Config::MAX_CUBS_NUMBER) ? true : false;
}

int LotteryValidModel::update(const PropertyType property, const int score) {
  return _scoreTable.set(property, score);
}

int LotteryValidModel::calculate() {
  int score =  calculate(static_cast<int>(_linkedIndex.size()));
  score = this->update(_currentProperty, score);
  return score;
}

int LotteryValidModel::calculate(const int linkedNumber) {
  float score = 0;
  
  for(int i = 1; i <= linkedNumber ; i ++) {
    score += internalCalculate(linkedNumber);
  }
  
  score = ceil(score);
  
  return score;
}

float LotteryValidModel::internalCalculate(const int linkedNumber) {
  const float basicScore = 1.0;
  float bonus = linkedNumber % Config::MAX_CUBS_NUMBER;
  
  return basicScore + internalCalculated(bonus);
}

float LotteryValidModel::internalCalculated(const float bonus) {
  if (bonus < 2) {
    return 0;
  }
  else if (bonus < 4) {
    return 0.3;
  }
  else if (bonus < 8) {
    return 0.5;
  }
  else {
    return 0.8;
  }
}

std::string LotteryValidModel::getPropertyName(PropertyType type) {
  switch(type) {
    case PropertyType::WISDOM :
      return PropertyName::PROPERTY_NAME_WISDOM;
    case PropertyType::ATTENTION :
      return PropertyName::PROPERTY_NAME_ATTENTION;
    case PropertyType::REACTION :
      return PropertyName::PROPERTY_NAME_REACTION;
    case PropertyType::VITALITY :
      return PropertyName::PROPERTY_NAME_VITALITY;
    default:
      return PropertyName::PROPERTY_NAME_WISDOM;
  }
}

// This method will change the _linkedIndex member.
void LotteryValidModel::swap() {
  
  for(int i = 0; i < Config::MAX_CUBS_NUMBER ; i ++) {
    if(_board.at(i) == PropertyType::NONE){
      swap(i, i + Config::MAX_CUBS_LINE, 0);
    }
  }
  
}

void LotteryValidModel::swap(int index1, int index2, int linkedIndex) {
  
  if(index2 >= Config::MAX_CUBS_NUMBER) {
    return;
  }
  else if(_board.at(index2) == PropertyType::NONE) {
    swap(index1, index2 + Config::MAX_CUBS_LINE, linkedIndex);
  }
  else {
    auto temp =  _board.at(index1);
    _board.at(index1) = _board.at(index2);
    _board.at(index2) = temp;
  }
  
}



