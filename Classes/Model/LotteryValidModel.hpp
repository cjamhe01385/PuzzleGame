//  Created by Yu-Shuan on 23/09/2017.
//
//

#ifndef LotteryValidModel_hpp
#define LotteryValidModel_hpp

#include <stdio.h>
#include <iostream>
#include <array>
#include <vector>
#include "EnumValue.hpp"
#include "Config.hpp"
#include "PropertyTable.hpp"
#include "SchoolTable.hpp"

class LotteryValidModel {

  public :
  
  /* Functions */
  void setup();
  void reset();
  
  void setupPropertyWithRandom();
  void setupPropertyWithManual(std::array<PropertyType, Config::MAX_CUBS_NUMBER>& board);
  
  const std::array<PropertyType, Config::MAX_CUBS_NUMBER>& getBoard(){ return _board; }
  const std::vector<int>& getLinkedIndex();
  
  PropertyType intToProperty(int intNumber);
  void linkSameProperty(int boardIndex);
  void link(int boardIndex);
  bool isValid(int index);
  void swap();
  void swap(int index1, int index2, int linkedIndex);
  int calculate();
  int calculate(const int linkedNumber);
  float internalCalculate(const int linkedNumber);
  float internalCalculated(const float linkedNumber);
  int update(const PropertyType property, const int score);
  std::string getPropertyName(PropertyType type);
  void updateProperty();
  bool isUpgrade() { return isUpgraded; };
  void UpgradeTrue() {isUpgraded = true;};
  void UpgradeFalse() {isUpgraded = false;};
  void updateLinkedIndex();
  SchoolData* getSchool();
  
  bool validProperty(PropertyType comparedProperty);
  void linkToArray(int boardIndex);
  
  PropertyType getCurrentProperty() { return _currentProperty; };
  
  std::array<PropertyType, Config::MAX_CUBS_NUMBER> _board;
  std::vector<int> _linkedIndex;
  PropertyTable _scoreTable;
  SchoolTable _schooTable;
  PropertyType _currentProperty;
  bool isUpgraded;
  SchoolData* _currentSchool;
};

#endif /* LotteryValidModel_hpp */
