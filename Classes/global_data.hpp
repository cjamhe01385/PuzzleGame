//  Created by Yu-Shuan on 29/06/2017.
//
//

#ifndef global_data_hpp
#define global_data_hpp

#include <stdio.h>

using namespace cocos2d;

class GameData : public Ref {

  public:
  void setName(std::string name);
  
  
  private:
  std::string character_name_;
  int gender_;
  int language_property_;
  int math_property_;
  int role;
  int revenue;

};

#endif /* global_data_hpp */
