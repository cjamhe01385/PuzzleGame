//
//  Created by Yu-Shuan on 20/09/2017.
//
//

#ifndef LotteryCubeLayer_hpp
#define LotteryCubeLayer_hpp

#include "EnumValue.hpp"

#include <stdio.h>
#include <string>
#include <memory>
#include <iostream>
#include <vector>
#include <array>
#include "Config.hpp"

USING_NS_CC;

class LotteryValidModel;
class LotteryCubeLayer : public Layer {

  public:
  CREATE_FUNC(LotteryCubeLayer);
  bool init() override;
 
  const Vector<Sprite*>& getCubes(){return _cubeSprite;};
  void setup(const std::array<PropertyType, Config::MAX_CUBS_NUMBER>& board);
  
  void highlightAction(const std::vector<int>& linkedIndex, PropertyType property);
  void cleanAction(const std::vector<int>& linkedIndex);
  void moveDownAction();
  void resetAction(const std::array<PropertyType, Config::MAX_CUBS_NUMBER>& board);
  
  private :
  std::shared_ptr<LotteryValidModel> _model;
  Sprite* setupCube(PropertyType property);
  void setupTexture(Sprite* cube, PropertyType property);
  const std::string& getActivityTexture(PropertyType property);
  
  
  void swap(int index, int index2);
  
  Vector<Sprite*> _cubeSprite;
};

#endif /* LotteryCubeItem_hpp */
